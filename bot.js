const Eve = require('./core/Eve');

const auth = require('./auth');
const { log } = require('./util/BotUtil');

const isDev = process.env.NODE_ENV === 'dev';

if (isDev) {
	log('----- DEV MODE -----');
}

const bot = new Eve({
	// Auth
	token: 'Bot ' + (isDev ? auth.devToken : auth.loginToken),
	sql: auth.sql
}, {
	// Client Options
	defaultImageFormat: 'png',
	intents: [
		'guilds',
		'guildMessages',
		// 'guildMessageReactions',
		'guildPresences',
		'guildVoiceStates',
		1 << 15 // 'messageContent'
	]
}, {
	// Command Client Options
	defaultCommandOptions: { guildOnly: true },
	defaultHelpCommand: false,
	description: 'Aeryk\'s personal Discord bot. Revision 2',
	owner: 'Aeryk',
	prefix: '~' // TODO: Another prefix, but for admin commands
});

process.on('unhandledRejection', (reason, promise) =>
	log(`Unhandled Promise: ${promise.name || '(anonymous Promise)'}\n\n${reason}`));

bot.connect();
