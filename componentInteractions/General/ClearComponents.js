module.exports = {
	name: 'clear-components',
	/**
	 * @arg {import('eris').ComponentInteraction} interaction
	 */
	run: (_, interaction) => {
		return interaction.editParent({ components: [] });
	}
};
