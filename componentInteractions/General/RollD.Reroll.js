const { argFormat, roll, rollResult } = require('../../commandInteractions/General/RollD.js');

module.exports = {
	name: 'rerolld',
	/**
	 * @arg {import('eris').ComponentInteraction} interaction
	 * @arg {string[]} args
	 * @arg {string} args[0]
	 */
	run: (_, interaction, [expr]) => {
		const [, numStr, sizeStr] = argFormat.exec(expr);
		const num = parseInt(numStr, 10);
		const size = parseInt(sizeStr, 10);

		const out = roll(num, size).join(', ');

		return interaction.editParent(rollResult(out, expr));
	}
};
