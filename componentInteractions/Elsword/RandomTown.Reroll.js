const { rollResult, randomLocation } = require('../../commandInteractions/Elsword/RandomTown.js');

module.exports = {
	name: 'rerandomtown',
	/**
	 * @arg {import('eris').ComponentInteraction} interaction 
	 * @arg {string[]} args 
	 * @returns {Promise<void>}
	 */
	run: (_, interaction, args) => {
		const includeRestAreas = args[0] === 'true';
		
		const location = randomLocation(includeRestAreas);
		
		return interaction.editParent(rollResult(location.name, includeRestAreas));
	}
};
