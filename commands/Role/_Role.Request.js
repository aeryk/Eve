const { Constants: { ApplicationCommandOptionTypes: OptionTypes } } = require('eris');

module.exports = {
	name: 'request',
	description: 'Request a personal role',
	options: [
		{
			name: 'name',
			description: 'Name of your personal role. Can be modified later.',
			type: OptionTypes.STRING,
			max_length: 100,
			required: true
		},
		{
			name: 'color',
			description: 'Optional. Color of your personal role. Can be modified later.',
			type: OptionTypes.STRING,
			max_length: 7
		}
	]
};
