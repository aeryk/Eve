const { Constants } = require('eris');

module.exports = {
	name: 'myrole',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	description: 'Role system commands',
	options: [
		'Check',
		'Color',
		'Name',
		'Request',
	].map(n => ({
		...require(`./_Role.${n}.js`),
		type: Constants.ApplicationCommandOptionTypes.SUB_COMMAND
	}))
};
