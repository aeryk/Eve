const { Constants } = require('eris');

module.exports = {
	name: 'kc2ed',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	scope: 'global',
	description: 'Convert K-Ching to ED with specified conversion rate',
	options: [{
		name: 'value',
		description: 'KChing value to be converted. Accepts shorthand [k]',
		type: Constants.ApplicationCommandOptionTypes.STRING,
		required: true
	}, {
		name: 'ed-rate',
		description: 'Rate of x million ED per 100 KC',
		type: Constants.ApplicationCommandOptionTypes.NUMBER,
		min_value: 0.000001,
		required: true
	}]
};
