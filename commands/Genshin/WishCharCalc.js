const { Constants } = require('eris');

module.exports = {
	name: 'wc',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	description: [
		'Calculates the amount of rolls and determines possible pity and pull amount.',
		'Input is not stored'
	].join(' '),
	options: [{
		name: 'primogems',
		description: 'Number of Primogems in your account',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false,
	}, {
		name: 'fates',
		description: 'Number of Acquainted/Intertwined Fates in your account',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false,
	}, {
		name: 'starting-pity',
		description: 'Number of pulls since your last 5*',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false,
	}]
};
