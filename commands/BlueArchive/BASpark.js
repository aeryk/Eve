const { Constants } = require('eris');

module.exports = {
	name: 'spark-ba',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	description: [
		'Calculates the amount of rolls and tells if a spark is possible or not.',
		'Input is not stored.'
	].join(' '),
	options: [{
		name: 'pyroxenes',
		description: 'Current pyroxene amount',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: true
	}, {
		name: 'single-tix',
		description: 'Current single draw tickets, if any',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false
	}, {
		name: 'ten-tix',
		description: 'Current 10-draw tickets, if any',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false
	}, {
		name: 'starting-sparks',
		description: 'Starting amount of sparks, if any',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false
	}]
};
