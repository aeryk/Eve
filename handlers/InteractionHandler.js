const { Constants, CommandInteraction, ComponentInteraction } = require('eris');

module.exports = {
	name: 'interactionCreate',
	/**
	 * @arg {import('../core/Eve')} bot
	 */
	f: async (bot, interaction) => {
		if (interaction instanceof CommandInteraction) {
			const names = [interaction.data.name];
			const { SUB_COMMAND_GROUP, SUB_COMMAND } =
				Constants.ApplicationCommandOptionTypes;
			let options = {};

			/**
			 * @arg {import('eris').CommandInteractionData} data 
			 * @returns {void}
			 */
			function getOptions (data) {
				for (const o of data.options) {
					if (o.type === SUB_COMMAND_GROUP || o.type === SUB_COMMAND) {
						options = {};
						names.push(o.name);
						const deeperData = o.type === SUB_COMMAND ? o : o.options[0];
						return getOptions(deeperData);
					} else
						options[o.name] = o.value;
				}
			}

			interaction.data.options && getOptions(interaction.data);

			bot.commandInteractions[names.join(' ')].run(bot, interaction, options);
		}

		if (interaction instanceof ComponentInteraction) {
			const { BUTTON, SELECT_MENU } = Constants.ComponentTypes;

			if (interaction.data.component_type === BUTTON) {
				const [name, ...args] = interaction.data.custom_id.split(' ');
				bot.componentInteractions[name].run(bot, interaction, args);
			}

			if (interaction.data.component_type === SELECT_MENU) {
				const name = interaction.data.custom_id;
				const values = interaction.data.values;
				bot.componentInteractions[name].run(bot, interaction, values);
			}
		}
	}
};
