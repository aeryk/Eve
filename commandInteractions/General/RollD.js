const { Constants } = require('eris');

const MAX_NUM = 100;
const MAX_SIZE = 1000;

const argFormat = /^(\d+)d(\d+)$/;

function roll (num, size) {
	return Array.from({ length: num }, () => {
		const val = Math.ceil(Math.random() * size);
		return val === size ? `**${val}**` : val;
	});
}

/**
 * @arg {string} result
 * @arg {string} expr
 */
function rollResult (result, expr) {
	return {
		content: result,
		components: [{
			type: Constants.ComponentTypes.ACTION_ROW,
			components: [
				{
					type: Constants.ComponentTypes.BUTTON,
					style: Constants.ButtonStyles.PRIMARY,
					custom_id: `rerolld ${expr}`,
					label: 'Reroll'
				},
				{
					type: Constants.ComponentTypes.BUTTON,
					style: Constants.ButtonStyles.SUCCESS,
					custom_id: 'clear-components',
					label: 'Settle'
				}
			]
		}]
	};
}

module.exports = {
	name: 'rolld',
	/**
	 * @arg {import('../../core/Eve')} bot
	 * @arg {import('eris').CommandInteraction} interaction
	 * @arg {{ expr: string }} args
	 * @arg {string} args.expr
	 * @returns {Promise<void> | void}
	 */
	run: (bot, interaction, { expr }) => {
		if (argFormat.test(expr) != null) {
			const [, numStr, sizeStr] = argFormat.exec(expr);
			const num = parseInt(numStr, 10);
			const size = parseInt(sizeStr, 10);

			if (num > MAX_NUM || size > MAX_SIZE) {
				return interaction.createMessage({
					content: 'Exceeded limit. Max is 100d1000',
					flags: Constants.MessageFlags.EPHEMERAL
				});
			}

			const out = roll(num, size).join(', ');

			return interaction.createMessage(rollResult(out, expr));
		}
	},
	argFormat,
	roll,
	rollResult
};
