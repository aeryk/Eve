const { commaPad } = require('../../util/StringUtil.js');
const emojis = require('../../core/emojis.json');

function getEmoji (s) {
	return emojis[s.toLowerCase().replace(' ', '_')];
}

function toStacks (n, s) {
	if (n < s) return n;
	let out = `${n / s | 0}s`;
	if (n % s > 0)
		out += ` ${commaPad(n % s)}`;
	return out;
}

const PresetRigo = {
	type: 'rigo',
	mat: 'Spectral Amethyst',
	matsPerHit: 3,
	altMat: 'Glaciem',
	pities: [
		10, 10, 10, 10, 10, 10, 10, 10, 10, // R 0 - R 9
		10, 10, 10, 10, 10, 10,             // R 9 - R15
		10, 50, 100, 20, 91, 180            // R15 - R21
	]
};

const _TeneExaPities = [
	5, 10, 15, 5, 10, 15, 5, 15, 20, // R 0 - R 9
	10, 15, 20, 20, 31, 40,          // R 9 - R15
	40, 80, 160, 40, 120, 240        // R15 - R21
];

const PresetTene = {
	type: 'tene',
	mat: 'Tenebrous Aura',
	matsPerHit: 15,
	altMat: 'Tasma Aura',
	pities: _TeneExaPities
};

const PresetExa = {
	type: 'exa',
	mat: 'Refined Exikel',
	matsPerHit: 20,
	altMat: 'Zetrium Steel',
	pities: _TeneExaPities
};

const Presets = [
	PresetRigo,
	PresetTene,
	PresetExa
];

function totalPity (pities, start, target) {
	let sum = 0;
	for (let i = start; i < target; i++) {
		sum += pities[i];
	}
	return sum;
}

/**
 * @arg {Presets[number]} preset
 * @arg {number} start
 * @arg {number} startingPity
 * @arg {number} target
 * @arg {boolean} altMat
 * @arg {number} blacksmith
 * @arg {number} edTix
 */
function calc (
	preset,
	start = 0,
	startingPity = 0.0,
	target,
	altMat = false,
	blacksmith = 0,
	edTix = 0
) {
	let mats = 0;
	let altMats = 0;
	let crystals = 0;
	let ed = 0; // by millions
	let edSavedTix = 0;
	let edSavedBs = 0;

	// round pity arg to the nearest actual bar value
	const realPity = Math.round(startingPity / 100 * preset.pities[start]) / preset.pities[start] * 100;
	const hitsSoFar = Math.floor(realPity / (100 / preset.pities[start]));
	const maxAttempts = totalPity(preset.pities, start, target) - hitsSoFar;

	calc: {
		if (start >= target) {
			break calc;
		}

		const hitPool = [];
	
		if (start < 9) {
			const cutoff = 9;
			const t1Ceil = Math.min(cutoff, target);
			const deduction = start < cutoff ? hitsSoFar : 0;
			const skillAdd = start < 6 && target >= 6 ? 1 : 0;
			const hits = totalPity(preset.pities, start, t1Ceil) - deduction + skillAdd;
			hitPool[2] = hits;
			
			mats += preset.matsPerHit * hits;
			altMats += 2000 * hits;
			crystals += 100 * hits;
			ed += 0.3 * hits;
		}
		
		if (start < 15) {
			const cutoff = 15;
			const t2Floor = Math.max(9, start);
			const t2Ceil = Math.min(cutoff, target);
			const deduction = start >= 9 && start < cutoff ? hitsSoFar : 0;
			const skillAdd = start < 12 && target >= 12 ? 1 : 0;
			const hits = totalPity(preset.pities, t2Floor, t2Ceil) - deduction + skillAdd;
			hitPool[1] = hits;
	
			mats += (preset.matsPerHit * 2) * hits;
			altMats += 4000 * hits;
			crystals += 200 * hits;
			ed += 0.9 * hits;
		}
	
		if (target > 15) {
			const t3Floor = Math.max(15, start);
			// const deduction = hitsSoFar;
			// TENE+ ONLY
			const skillAdd = preset.type !== 'rigo' && start < 18 && target >= 18 ? 1 : 0;
			const hits = totalPity(preset.pities, t3Floor, target) - hitsSoFar + skillAdd;
			hitPool[0] = hits;
	
			mats += (preset.matsPerHit * 3) * hits;
			altMats += 6000 * hits;
			crystals += 300 * hits;
			ed += 1.8 * hits;
		}
	
		// ED Ticket calc goes here
		if (edTix > 0) {
			let edTixLeft = edTix;

			[1.8, 0.8, 0.3].forEach((n, i) => {
				if (hitPool[i] != undefined) {
					const uses = Math.min(edTixLeft, hitPool[i]);
					edTixLeft = Math.max(edTixLeft - uses, 0);
					edSavedTix += uses * n;
				}
			});

			ed -= edSavedTix;
		}

		if (blacksmith > 0) {
			edSavedBs = ed * (0.01 * blacksmith);
			ed -= edSavedBs;
		}

		// js being funny with decimals
		ed = Math.round(ed * 10) / 10;

		if (ed === (ed | 0)) ed = ed | 0; // remove decimal if none
	}

	return {
		// mats
		mats, altMats, crystals, ed,
		// args
		preset, start, startingPity, target, altMat, blacksmith, edTix,
		// other
		realPity, hitsSoFar, maxAttempts, edSavedTix, edSavedBs
	};
}

function toEmbed ({
	// mats
	mats, altMats, crystals, ed,
	// args
	preset, start, _startingPity, target, altMat, blacksmith, edTix,
	// other
	realPity, _hitsSoFar, maxAttempts, edSavedTix, edSavedBs
}, author) {
	const fields = [];

	/**
	 * @arg {number} ed 
	 * @returns {string}
	 */
	function edString (ed) {
		let out = '';

		function format (ed) {
			let out = ' ';
			if (ed >= 1000) {
				out += ((ed / 1000) | 0) + 'b ';
			}
	
			if (ed % 1000 > 0) {
				out += Math.round((ed % 1000) * 100) / 100 + 'm ';
			}
	
			if (ed === 0) {
				out += '0 ';
			}
	
			return getEmoji('ed') + out;
		}

		out += format(ed);

		if (edTix > 0) {
			out += `\n-# ${format(edSavedTix)} saved from ${edTix} tickets`;
		}

		if (blacksmith > 0) {
			out += `\n-# ${format(edSavedBs)} saved from ${blacksmith}% Blacksmith`;
		}

		return out;
	}

	if (start < target) {
		fields.push({
			name: 'Target',
			value: [
				`R${start} ${realPity > 0 ? '(' + (realPity.toFixed(3)) + '%) ' : ''}→ R${target}`,
				`Max Attempts: ${commaPad(maxAttempts)}`
			].join('\n')
		});

		const [ amt, lbl ] = altMat ? [ altMats, preset.altMat ] : [ mats, preset.mat ];

		fields.push({
			name: 'Materials',
			value: [
				`${getEmoji(lbl)} ${commaPad(amt)} (${toStacks(amt, 9999)}) ${lbl}`,
				`${getEmoji('magical crystal')} ${commaPad(crystals)} (${toStacks(crystals, 1e4)}) Magical Crystals`,
				edString(ed)
			].join('\n')
		});
	}

	const embeds = [{
		color: preset.type.startsWith('tene') ? 0x45D298 : 0x76DBEC,
		timestamp: new Date(Date.now()).toISOString(),
		footer: {
			text: 'Calculated at'
		},
		author: {
			name: `${author.username}'s reforge`,
			icon_url: author.staticAvatarURL
		},
		fields
	}];

	return { embeds };
}

function reforgeData (
	type,
	start = 0,
	startingPity = 0.0,
	target,
	altMat = false,
	blacksmith,
	edTix
) {
	const preset = Presets.find(p => p.type === type);
	return calc(preset, start, startingPity, target, altMat, blacksmith, edTix);
}

function reforge (
	author,
	type,
	start = 0,
	startingPity = 0.0,
	target,
	altMat = false,
	blacksmith,
	edTix
) {
	const data = reforgeData(type, start, startingPity, target, altMat, blacksmith, edTix);

	return toEmbed(data, author);
}

module.exports = {
	reforge,
	reforgeData
};
