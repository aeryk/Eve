const { reforge } = require('../_util/ReforgeUtil.js');

module.exports = {
	name: 'reforge',
	run: (_, interaction, args) => {
		const {
			type = 'tene',
			target,
			start = target - 1,
			'starting-pity': startingPity = 0.0,
			'alt-mat': altMat = false,
			blacksmith = 0,
			'ed-tix': edTix = 0
		} = args;

		const { embeds } = reforge(interaction.member.user, type, start, startingPity, target, altMat, blacksmith, edTix);

		return interaction.createMessage({ embeds });
	}
};
