const { Constants } = require('eris');

const towns = [
	{ name: 'Ruben', blacksmith: 'Ann' },
	{ name: 'Elder', blacksmith: 'Lenphad' },
	{ name: 'Bethma', blacksmith: 'Toma' },
	{ name: 'Dice Sky Road', restArea: true },
	{ name: 'Altera', blacksmith: 'Agatha' },
	{ name: 'Feita', blacksmith: 'Lento' },
	{ name: 'Velder', blacksmith: 'Hanna' },
	{ name: 'Clock Tower Square', restArea: true },
	{ name: 'Hamel', blacksmith: 'Horatio' },
	{ name: 'Fountain of Healing', restArea: true },
	{ name: 'Sander', blacksmith: 'Dafarr' },
	{ name: 'Wind Stone Ruins', restArea: true },
	{ name: 'Lanox', blacksmith: 'Steel & Sdeing' },
	{ name: 'Silent Night Shelter', restArea: true },
	{ name: 'Atlas Station', restArea: true },
	{ name: 'Elysion', blacksmith: 'Durenda' },
	{ name: 'Elrianode', blacksmith: 'Equipment Refiner' },
	{ name: 'Camp: Crimson Edge', restArea: true },
	{ name: 'Camp: Aurora', restArea: true },
	{ name: 'Magmelia', blacksmith: 'Betty' }
];

function rollResult (result, includeRestAreas) {
	return {
		content: result,
		components: [{
			type: Constants.ComponentTypes.ACTION_ROW,
			components: [{
				type: Constants.ComponentTypes.BUTTON,
				style: Constants.ButtonStyles.PRIMARY,
				custom_id: `rerandomtown ${includeRestAreas}`,
				label: 'Reroll'
			}, {
				type: Constants.ComponentTypes.BUTTON,
				style: Constants.ButtonStyles.SUCCESS,
				custom_id: 'clear-components',
				label: 'Clear Buttons'
			}]
		}]
	};
}

function randomLocation (includeRestAreas) {
	const areas = towns.filter(t => {
		return includeRestAreas || !(t.restArea ?? false);
	});

	return areas[Math.floor(Math.random() * areas.length)];
}

module.exports = {
	name: 'randomtown',
	run: (_, interaction, args) => {
		const {
			'rest-area': includeRestAreas = false
		} = args;

		const location = randomLocation(includeRestAreas);

		return interaction.createMessage(rollResult(location.name, includeRestAreas));
	},
	rollResult,
	randomLocation
};
