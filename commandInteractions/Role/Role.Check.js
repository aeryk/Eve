const { Constants } = require('eris');
const { getSoloRole } = require('./_RoleUtil');

module.exports = {
	name: 'myrole check',
	/**
	 * @arg {import('../../code/Eve.js')} bot
	 * @arg {import('eris').CommandInteraction<import('eris').TextChannel>} interaction
	 * @returns {Promise<void>}
	 */
	run: async (bot, interaction) => {
		/** @returns {string} */
		function checkRole () {
			if (!interaction.appPermissions.has(Constants.Permissions.manageRoles))
				return 'No **Manage Roles** permission.';
			
			const soloRole = getSoloRole(interaction.member);
			if (!soloRole)
				return 'No personal role.';
			else {
				return `${soloRole.name} (#${('0000' + soloRole.color.toString(16)).slice(-6)})`;
			}
		}

		return interaction.createMessage({
			content: checkRole(),
			flags: Constants.MessageFlags.EPHEMERAL
		});
	}
};
