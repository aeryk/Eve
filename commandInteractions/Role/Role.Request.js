const { Constants } = require('eris');
const { preCheck, findColorableRolePos } = require('./_RoleUtil');

module.exports = {
	name: 'myrole request',
	/**
	 * @arg {import('../../code/Eve.js')} bot
	 * @arg {import('eris').CommandInteraction<import('eris').TextChannel>} interaction 
	 * @arg {{'name': string, color: string | undefined}} args
	 * @returns {Promise<void>}
	 */
	run: async (bot, interaction, args) => {
		const { member } = interaction;
		
		/** @returns {string} */
		async function assignPersonalRole () {
			const pr = preCheck(interaction, true);
			if (pr.canManage || pr.hasSoloRole) return pr.message;

			let colorStr = args.color ?? '99aab5';
			const validColor = /^#?((?:[\dA-F]{3}){1,2})$/i;
			if (!validColor.test(colorStr))
				return `Invalid color format: '${colorStr}', use hex.`;
			if (colorStr.length === 3)
				// expand 3 digit
				colorStr = colorStr.replace(/([\dA-F])/gi, '$1$1');
			
			const newRole = await member.guild.createRole({
				color: parseInt(colorStr.match(validColor)[1], 16),
				name: args.name
			});

			await newRole.editPosition(findColorableRolePos(bot, member));

			await member.addRole(newRole.id, 'Request command');
		}

		return interaction.createMessage({
			content: await assignPersonalRole(),
			flags: Constants.MessageFlags.EPHEMERAL
		});
	}
};
