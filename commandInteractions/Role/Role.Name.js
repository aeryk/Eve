const { Constants } = require('eris');
const { preCheck, canEditRole } = require('./_RoleUtil');

module.exports = {
	name: 'myrole name',
	/**
	 * @arg {import('../../code/Eve.js')} bot
	 * @arg {import('eris').CommandInteraction<import('eris').TextChannel>} interaction 
	 * @arg {{'new-name': string | undefined}} args
	 * @returns {Promise<void>}
	 */
	run: async (bot, interaction, args) => {
		/** @returns {string} */
		async function changeRoleName () {
			const pr = preCheck(interaction);
			if (pr.canManage || !pr.hasSoloRole) return pr.message;

			const newName = args['new-name'];
			if (!newName) return pr.soloRole.name;

			if (newName === pr.soloRole.name)
				return 'Same name. No change made.';
	
			if (canEditRole(bot, pr.soloRole)) {
				await pr.soloRole.edit({ name: newName }, 'Name change command');
				return 'Role name changed.';
			} else {
				return 'Insufficient permission to modify your personal role.';
			}
		}

		return interaction.createMessage({
			content: await changeRoleName(),
			flags: Constants.MessageFlags.EPHEMERAL
		});
	}
};
