const { spark } = require('../_util/Sparks.js');
const { getInvoker } = require('../../util/InteractionUtil.js');

module.exports = {
	name: 'spark-ba',
	/**
	 * @arg {import('eris').CommandInteraction} interaction 
	 * @arg {{[n: string]: any}} args 
	 * @returns {Promise<void>}
	 */
	run: (_, interaction, args) => {
		const {
			pyroxenes = 0,
			'single-tix': oneTix = 0,
			'ten-tix': tenTix = 0,
			'starting-sparks': startingSparks = 0
		} = args;
		
		const { embeds } = spark('BA')(pyroxenes, oneTix, tenTix, startingSparks)
			.embed(getInvoker(interaction));
		
		return interaction.createMessage({ embeds });
	}
};
