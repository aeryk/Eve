const fs = require('fs');
const path = require('path');

const { QueryTypes } = require('sequelize');

class EvalContext {
	/**
	 * @arg {import('../core/Eve.js')} bot
	 * @arg {import('eris').Message<import('eris').TextChannel>} msg
	 */
	constructor (bot, msg) {
		this.bot = bot;
		this.member = msg.member;
		this.channel = msg.channel;
		this.guild = msg.channel.guild;
	}

	async readQuery (s) {
		return this.bot.sql.query(s, { type: QueryTypes.SELECT });
	}

	async query (s) {
		return this.bot.sql.query(s);
	}
}

module.exports = {
	fs, path, EvalContext
};
