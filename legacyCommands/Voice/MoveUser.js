const { verifyMoveQuery } = require('./_voiceUtil');

module.exports = {
	label: 'moveMe',
	/**
	 * @arg {import('eris').Message} msg
	 * @arg {string[]} args
	 */
	generator: (_, msg, args) => {
		const { error, destinationVc } = verifyMoveQuery(args.join(' '), msg);

		if (error)
			return error;
		else
			msg.member.edit({ channelID: destinationVc.id });
	},
	options: {
		argsRequired: true,
		usage: 'moveUs <vc name or id>'
	}
};
