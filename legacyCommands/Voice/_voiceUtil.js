const ChannelTypes = {
	GuildText: 0,
	DMText: 1,
	GuildVoice: 2,
	GroupDM: 3,
	GuildCategory: 4,
	GuildNews: 5,
	GuildStore: 6
	// GuildNewsThread: 10,
	// GuildPublicThread: 11,
	// GuildPrivateThread: 12,
	// GuildStageVoice: 13
};

/**
 * @arg {string} a name or id of vc
 * @arg {object} param1 
 * @arg {import('eris').Member} param1.member
 * @arg {import('eris').VoiceChannel} param1.channel
 * @returns 
 */
function verifyMoveQuery (a, { member, channel }) {
	/*
	VC Types
	0: Guild Text
	1: DM Text
	2: Guild Voice
	3: Group DM
	4: Guild Category
	*/
	const voiceChannels = channel.guild.channels.filter(g => g.type === ChannelTypes.GuildVoice);

	const senderVcId = member.voiceState.channelID;

	const senderVc = voiceChannels.find(vc => vc.id === senderVcId);
	if (!senderVc) return { error: 'You\'re not in a voice channel.' };

	const destinationVc = voiceChannels.find(vc => vc.name === a || vc.id === a);
	if (!destinationVc) return { error: `There is no voice channel with the name or id \`${a}\`` };

	return { senderVc, destinationVc };
}

module.exports = {
	verifyMoveQuery
};
