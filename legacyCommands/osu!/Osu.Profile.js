const osu = require('node-osu');

const ou = require('./_OsuUtil');

const osuApi = new osu.Api(process.env.OSU_KEY || require('../../auth').osuKey, {
	notFoundAsError: true,
	completeScores: false
});

/*
 * Revised from https://github.com/aeryk17aj/EveKai/blob/094d9966a35ef35956fdc6b1cb56adce088705fd/plugins/osu.js#L74-L149
 */
module.exports = {
	label: 'profile',
	generator: (_, msg, [ uname, mode = 0 ]) => {
		const modes = ['osu!', 'osu!taiko', 'osu!catch', 'osu!mania'];

		osuApi.getUser({ u: uname, m: mode }).then(user => {
			msg.channel.createMessage({ embeds: [{
				title: `${modes[mode]} stats` + (mode > 1 ? '(:warning: WIP)' : ''),
				description: ou.basicStats(user),
				color: 0xFFB2C5,
				thumbnail: { url: 'https://a.ppy.sh/' + user.id },
				author: {
					name: user.name,
					icon_url: 'https://a.ppy.sh/' + user.id,
					url: 'https://osu.ppy.sh/users/' + user.id
				},
				fields: [{
					name: 'Clears',
					value: ou.clearCounts(user),
					inline: true
				}, {
					name: 'Hits',
					value: ou.hits(user, mode),
					inline: true
				}, {
					name: 'Accuracy',
					value: ou.accuracies(user, mode),
					inline: true
				}]
			}]});
		});
	},
	options: {
		aliases: [ 'user' ],
		argsRequired: true,
		hooks: {
			// Transform mode argument string to just a number
			postCheck: (_, args) => {
				if (!args[1])
					args[1] = 0;
				else {
					switch (args[1].toLowerCase()) {
						case 'osu': case 'standard':
							args[1] = 0; break;
						case 'taiko':
							args[1] = 1; break;
						case 'catch': case 'fruits':
							args[1] = 2; break;
						case 'mania':
							args[1] = 3; break;
						default:
							args[1] = 0;
					}
				}
				
				return { args };
			}
		},
		usage: [
			'Displays an osu! user\'s profile with an optional mode argument.',
			'',
			'Format: osu profile <ign> [mode, default osu]',
			'Accepted modes: *osu*, *standard*, *taiko*, *catch*, *fruits*, *mania*'
		].join('\n')
	}
};
