/**
 * Ported from https://github.com/aeryk17aj/EveKai/blob/094d9966a35ef35956fdc6b1cb56adce088705fd/plugins/osu.js#L151-L161
 * 
 * Changes
 * Simplify regex using ignore case flag (i)
 * Add argument check
 */
const usage =
	'~tvis [ dDkK]...\n' +
	'd and k for don and kat, respectively\n' +
	'D and K for finishers.\n' +
	'Space for separation';

module.exports = {
	label: 'tvis',
	
	/**
	 * @arg {string[]} args
	 */
	generator: (_, __, args) => {
		const a = args.join(' ');
		const match = /[ dk]/gi;
		if (match.test(a)) {
			return a.replace(match, b => ({
				'k': '\u{1F535}',	// k -> blue circle
				'd': '\u{1F534}',	// d -> red circle
				'K': '(\u{1F535})',	// K -> blue circle in brackets
				'D': '(\u{1F534})',	// D -> red circle in brackets
				' ': '   ' // more spaces
			})[b]);
		} else
			return usage;
	},
	options: {
		argsRequired: true,
		usage
	}
};
