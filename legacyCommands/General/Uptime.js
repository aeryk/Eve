const { duration } = require('../../util/StringUtil');

module.exports = {
	label: 'uptime',
	generator: () => `I've been up for ${duration(process.uptime())}`
};
