function processDraws (pityPreset, primos, fates = 0, startPity = 0, ) {

	const primoDraws = Math.floor(primos / 160);
	const draws = primoDraws + fates;
	const totalPity = draws + startPity;

	const fiveMin = Math.floor(totalPity / pityPreset.FiveHard);
	const fiveMax = Math.floor(totalPity / pityPreset.FiveSoft);
	// 5* pulls can offset 4* pity
	const fourMin = Math.floor((draws + (totalPity % 10) - fiveMax) / pityPreset.FourHard);
	const fourMax = Math.floor((draws + (totalPity % 10) - fiveMin) / pityPreset.FourSoft);

	return {
		// Args
		pityPreset, primos, fates, startPity,

		// Calculated draws and pity
		primoDraws, draws, totalPity,

		// Calculated 4*s and 5*s
		fiveMin, fiveMax,
		fourMin, fourMax,

		// Extra
		fiveSoft: fiveMax > fiveMin,
		fourSoft: fourMax > fourMin
	};
}

function toEmbed ({
	pityPreset, primos, fates, startPity,
	primoDraws, draws, totalPity,
	fiveMin, fiveMax,
	fourMin, fourMax
}, author) {
	
	// Output string start
	let out = `:sparkles: ${draws} (`;

	if (fourMin < fourMax) {
		out += `${fourMin}-`;
	}

	out += `${fourMax}x :purple_circle: | `;

	if (fiveMin < fiveMax) {
		out += `${fiveMin}-`;
	}

	out += `${fiveMax}x :yellow_circle:)\n`;
	// Output string end

	if (startPity > 0) {
		out += `Starting Pity: ${startPity} draws\n`;
	}

	if (totalPity < pityPreset.FiveHard && totalPity >= pityPreset.FiveSoft) {
		out += '**Pity possible**\n';
	} else if (totalPity < pityPreset.FiveSoft) {
		out += 'Pity not possible.\n';
	}

	out += `Total Pity: ${totalPity}`;

	let color = 0xFF0000; // Pity < Soft
	if (totalPity >= pityPreset.FiveHard)
		color = 0x00FF00; // Pity >= Hard
	else if (totalPity >= pityPreset.FiveSoft)
		color = 0xFFFF00; // Soft <= Pity < Hard

	const fields = [{
		name: 'Total draws',
		value: out
	}];

	if (primos > 160 && fates > 0) {
		fields.push({
			name: 'Breakdown',
			value: [
				`<:primogem:863309545631907870> ${primos} = :sparkles: ${primoDraws}`,
				`<:intertwinedFate:863310371791831051> ${fates} = :sparkles: ${fates}`
			].join('\n'),
			inline: true
		});
	}

	const embeds = [{
		color,
		timestamp: new Date(Date.now()).toISOString(),
		footer: {
			text: `Calculated at`
		},
		author: {
			// TODO: Less arbitrary way to tell if char or wep
			name: `${author.username}'s ${pityPreset.FiveHard === 90 ? 'character' : 'weapon'} wishes`,
			icon_url: author.staticAvatarURL
		},
		fields
	}];

	return { embeds };
}

// Following this infographic
// https://twitter.com/genshin_wishes/status/1409659194681163781

const PresetChar = {
	FourSoft: 9,
	FourHard: 10,
	FiveSoft: 74,
	FiveHard: 90
};

const PresetWep = {
	FourSoft: 8,
	FourHard: 9,
	FiveSoft: 63,
	FiveHard: 77
};

function wishCharData (primos, fates = 0, startPity = 0) {
	return processDraws(PresetChar, primos, fates, startPity);
}

function wishChar (author, primos, fates = 0, startPity = 0) {
	return toEmbed(wishCharData(primos, fates, startPity), author);
}

function wishWepData (primos, fates = 0, startPity = 0) {
	return processDraws(PresetWep, primos, fates, startPity);
}

function wishWep (author, primos, fates = 0, startPity = 0) {
	return toEmbed(wishWepData(primos, fates, startPity), author);
}

module.exports = {
	wishCharData, wishChar,
	wishWepData, wishWep
};
