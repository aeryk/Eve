const { log } = require('../util/BotUtil');
const { commaPad } = require('../util/StringUtil');

const { /* eslint-disable no-unused-vars */
	fs,
	path,
	EvalContext
} = require('./_EvalContext.js'); /* eslint-enable */

const { botAuthor } = require('./_Permissions');

module.exports = {
	label: 'eval',
	/**
	 * @arg {import('../core/Eve')} bot
	 * @arg {import('eris').Message} msg
	 * @arg {string[]} args
	 * @return {Promise}
	 */
	generator: async (bot, msg, args) => {
		/* eslint-disable no-unused-vars */
		const {
			query, readQuery, member, channel, guild
		} = new EvalContext(bot, msg);

		const client = bot; // alias
		const sender = member;
		/* eslint-enable */

		const code = args.join(' ');
		let result;
		try {
			result = eval(code);
			if (result === msg.content) return;
		} catch (e) {
			log(
				`Failed eval in ${msg.channel.guild.name} : ${msg.channel.name}\n\n${e}\n`
			);
			return 'It didn\'t work';
		}

		switch (typeof result) {
			case 'string':
			case 'boolean':
				return result;
			case 'number':
				return commaPad(result);
			case 'function':
				if (typeof result.then === 'function') {
					let res = await result;
					res = Array.isArray(res) ? res[0] : res;
					return JSON.stringify(res, null, 4);
				} else return result();
			default:
				if (Array.isArray(result)) return result.join(', ');
		}
	},
	/** @type {import('eris').CommandOptions} */
	options: {
		argsRequired: true,
		hidden: true,
		requirements: botAuthor
	}
};
