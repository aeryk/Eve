// const { log } = require('../../util/BotUtil');

/**
 * Represents a module containing commands and/or submodules
 *
 * @class LegacyCommandModule
 */
class LegacyCommandModule {
	/**
	 * @arg {string} name 
	 */
	constructor (name) {
		this.name = name;
		
		/** @type {Command[]} */
		this.commands = [];
		/** @type {Array<LegacyCommandModule>} */
		this.submodules = [];
	}

	/**
	 * @arg {string} label
	 * @arg {CommandGenerator} generator
	 * @arg {import('eris').CommandOptions} options
	 * @arg {Command[]} subcommands
	 */
	addCommand (label, generator, options = {}, subcommands = []) {
		this.commands.push({ label, generator, options, subcommands });
	}

	/**
	 * @arg {LegacyCommandModule} submodule The submodule
	 */
	addSubmodule (submodule) {
		this.submodules.push(submodule);
	}

	/**
	 * Adapt generator to include client as an argument
	 * 
	 * @arg {Eve} bot
	 * @arg {CommandGenerator} generator
	 * @returns {Function|string|Array<Function|string>|void}
	 */
	processGenerator (bot, generator) {
		if (typeof generator === 'string')
			return generator;
		else if (Array.isArray(generator)) {
			return generator.map(g => {
				if (typeof g === 'string')
					return g;
				else if (typeof g === 'function')
					return (msg, args) => g(bot, msg, args);
			});
		} else if (typeof generator === 'function')
			return (msg, args) => generator(bot, msg, args);
	}

	/**
	 * @arg {Eve} bot
	 */
	addToBot (bot) {
		this.commands.forEach(({ label, generator, options, subcommands }) => {
			const command = bot.registerCommand(label, this.processGenerator(bot, generator), options);
			this.addSubcommands(bot, command, subcommands || []);
		});

		// Recursively add submodules
		if (this.submodules.length)
			this.submodules.forEach(sm => sm.addToBot(bot));
	}

	/**
	 * @arg {Eve} bot
	 * @arg {Command} command
	 * @arg {Command[]} subcommands
	 */
	addSubcommands (bot, command, subcommands) {
		if (subcommands.length > 0) {
			subcommands.forEach(({ label, generator, options, subcommands: scq = [] }) => {
				const subCommand = command.registerSubcommand(label, this.processGenerator(bot, generator), options);

				// Recursively add subcommands
				if (scq.length > 0)
					this.addSubcommands(bot, subCommand, scq);
			});
		}
	}
}

/**
 * @typedef {import('eris').Command} Command
 * @typedef {import('eris').CommandGenerator} CommandGenerator
 * @typedef {import('../Eve')} Eve
 */

module.exports = LegacyCommandModule;
