const { getDirents } = require('../../util/FSUtil');

const files = getDirents('core/extension')
	.filter(di => di.isFile() && di.name.startsWith('_') && di.name.endsWith('.js'))
	.map(di => di.name);

// loosely based on
// https://github.com/minemidnight/eris-additions/blob/c687646e4a/index.js
module.exports = Eris => {
	files.forEach(e => require(`./${e}`)(Eris));
	return Eris;
};
