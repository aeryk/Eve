/**
 * StringUtil.js
 * These are my common functions for general use
 */

/**
 * Adds commas to every 3 characters / digits
 * 
 * @arg  {number|string} s - Number or string
 * @returns {string} - Now padded with commas
 */
function commaPad (s) {
	if (typeof s !== 'string') s = s.toString();
	return s.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ',');
}

/**
 * Gets codepoint from a unicode character in the form of a string
 * 
 * @arg {string} a Input unicode character
 * @returns {string} the found codepoint
 */
function getCodePoint (a) {
	a = a.replace(/\ufe0f|\u200d/gm, ''); // strips unicode variation selector and zero-width joiner
	const r = [];
	let index = 0;
	let currentChar = 0;
	let p = 0;
	while (index < a.length) {
		currentChar = a.charCodeAt(index++);
		if (p) {
			r.push((65536 + (p - 55296 << 10) + (currentChar - 56320)).toString(16));
			p = 0;
		} else if (55296 <= currentChar && currentChar <= 56319)
			p = currentChar;
		else r.push(currentChar.toString(16));
	}

	return r.join('-');
}

/**
 * Displays specified length of seconds to a readable format of:
 *
 * ###w #d #h ##m ##s
 *
 * @arg {number} ts Total seconds
 * @returns {string}
 */
function duration (ts) {
	const w = Math.floor(ts / 604800);
	const d = Math.floor(ts / 86400) % 7;
	const h = Math.floor(ts / 3600) % 24;
	const m = Math.floor(ts / 60) % 60;
	const s = Math.floor(ts) % 60;

	let out = `${s}s`;

	if (m > 0) out = `${m}m ${out}`;
	if (h > 0) out = `${h}h ${out}`;
	if (d > 0) out = `${d}d ${out}`;
	if (w > 0) out = `${w}w ${out}`;

	return out;
}

module.exports = {
	commaPad,
	getCodePoint,
	duration
};
