const Eris = require('eris');

/**
 * @arg {Eris.CommandInteraction | Eris.ComponentInteraction} inter
 * @returns {Eris.Member | Eris.User | undefined}
 */
function getInvoker (inter) {
	if (inter.channel.type === Eris.Constants.ChannelTypes.DM)
		return inter.user;
	else
		return inter.member;
}

module.exports = { getInvoker };
